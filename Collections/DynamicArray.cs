﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using Collections.Interfaces;

namespace Collections
{
	public class DynamicArray<T> : IDynamicArray<T>
	{
		private T[] array;


		
		public DynamicArray()
		{
			array = new T[8];
		}

		public DynamicArray(int capacity)
		{
			if (capacity < 0) throw new ArgumentOutOfRangeException(capacity.ToString());
			else { array = new T[capacity]; }

		}

		public DynamicArray(IEnumerable<T> items)
		{
			array = new T[1];
			int k = -1;
			foreach (var i in items)
			{
				k++;
				array[k] = i;
				Array.Resize(ref array, k + 2);
			}
			Array.Resize(ref array, k + 1);
			
		}

		public T this[int index]
		{
			get { if (index < this.Length) return array[index];
				else throw new IndexOutOfRangeException();
			}
			set
			{
				if (index < this.Length) array[index] = value;
				else throw new IndexOutOfRangeException();
			}
		}
		public IEnumerator<T> GetEnumerator()
		{
			return new ArrayEnum(array);

		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public int Length {
			get {
				return FindNotNullIndex(array);
			}
			
		}
		public int Capacity { 
			get
			{ return array.Length; }
			
		}
		public void Add(T item)
		{
			int nullIndex = FindNullIndex(array);

			if (nullIndex != -1)
			{ 
				array[nullIndex] = item;
			}
			else {
				int _temp = array.Length;
				GiveMeMoreElements(ref array);
				nullIndex = FindNullIndex(array);
				if (nullIndex >= 0)
					array[nullIndex] = item;
				else { array[_temp] = item; }
			}

		}

		//метод GiveMeMoreElements удваивает число элементов в массиве 
		public void GiveMeMoreElements<U>(ref U[] someArray)
		{
			if (someArray.Length == 0)
			{ Array.Resize(ref someArray, 1); }
			else { Array.Resize(ref someArray, 2 * someArray.Length); }
		}

		// метод FindNullIndex ищет первый пустой член массива
		public int FindNullIndex(T[] someArray)
		{
			int zero = 0;
			object o = zero;
			int result = -1;
			for (int i = 0; i < this.Capacity; i++)
			{
				try
				{
					if (someArray[i].Equals(o))
					{
						result = i;
						break;
					}
				}
				catch (NullReferenceException) { return i; } 
			}
			return result;
		}

		public int FindNotNullIndex<U>(U[] someArray) 
		{
			int notNull = 0;

            foreach (var i in someArray)
			{
				int zero = 0;
				object o = zero;
				try
				{
					if (i.Equals(o))
					{ notNull++; }
				}
				catch (NullReferenceException)
				{ notNull++; }
            }
			return this.Capacity - notNull;
				
        }
        public void AddRange(IEnumerable<T> items)
		{
			int nullIndex;

			foreach (var i in items)
			{
				nullIndex = FindNullIndex(array);

				if (nullIndex == -1) 
				{
					GiveMeMoreElements(ref array);
					nullIndex = FindNullIndex(array);
					array[nullIndex] = i;
				}
				else { array[nullIndex] = i; }

			}

		}

		public void Insert(T item, int index)
		{
			if (index > this.Capacity || index < 0)
			{ throw new IndexOutOfRangeException(); }
			else
            {
				int arrayTail = this.Capacity - index;
				if (this.Capacity == this.Length) 
                {													
					GiveMeMoreElements(ref array);					
					Array.Copy(array, index, array, index+1, arrayTail-1);
					array[index] = item;
                }
				else if (this.Capacity > this.Length)
                {
					Array.Copy(array, index, array, index + 1, arrayTail-1);
					array[index] = item;
				}

            }
			
		}

		public bool Remove(T item)
		{
			string localItem = item.ToString();
			for (int i=0; i < array.Length; i++)
            {
				if (string.Compare(array[i].ToString(), localItem) == 0)
				{
					Array.Copy(array, i + 1, array, i, array.Length - i-1);
					array[array.Length - 1] = default;
					break;
				}
				else if (i == array.Length - 1) { return false; }
            }
			return true;

		}

		public class ArrayEnum : IEnumerator<T>
		{
			private T[] array;
			private int position = -1;

			public ArrayEnum(T[] list)
			{ array = list; }

			public T Current
			{
				get
				{
					try { return array[position]; }
					catch (IndexOutOfRangeException) { throw; }
				}
			}

			object IEnumerator.Current
			{ get { return Current; } }
			public bool MoveNext()
			{
				position++;
				return (position < array.Length);
			}

			public void Reset()
			{
				position = -1;
			}

			void IDisposable.Dispose()
			{
			
			}
        }
	}


}